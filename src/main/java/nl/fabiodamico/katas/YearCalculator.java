package nl.fabiodamico.katas;

public class YearCalculator {

  public boolean isLeapYear(int year) {
    if (isDivisibleBy(year, 100) && !isDivisibleBy(year, 400)) {
      return false;
    }
    return isDivisibleBy(year, 4);
  }

  private boolean isDivisibleBy(int number, int divisor) {
    return number % divisor == 0;
  }
}
