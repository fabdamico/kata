package nl.fabiodamico.katas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class MyStack<T> {

  private final List<T> stackArrayList;

  public MyStack() {
    this.stackArrayList = new ArrayList<>();
  }

  public int getSize() {
    return stackArrayList.size();
  }

  public void push(T newItem) {
    stackArrayList.add(stackArrayList.size(), newItem);
  }

  public void pop() {
    if (stackArrayList.isEmpty()) {
      throw new IllegalStateException("The stack is empty. The operation cannot be performed");
    }
    stackArrayList.remove(stackArrayList.size() - 1);
  }

  public boolean contains(T elementToFind) {
    if (stackArrayList.isEmpty()) {
      throw new IllegalStateException("The stack is empty. The operation cannot be performed");
    }
    return stackArrayList.contains(elementToFind);
  }

  public T access(T item) {
    Predicate<T> equalityPredicate = value -> value.equals(item);
    if (contains(item)) {
      return stackArrayList
          .stream()
          .filter(equalityPredicate)
          .findAny()
          .get();
    }
    throw new IllegalArgumentException("No element in the stack for the given argument: " + item);
  }
}
