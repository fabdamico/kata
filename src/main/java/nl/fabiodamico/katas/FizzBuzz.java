package nl.fabiodamico.katas;

public class FizzBuzz {

  public String print(final int number) {
    boolean isDivisibleBy3 = number % 3 == 0;
    boolean isDivisibleBy5 = number % 5 == 0;
    if (number % 15 == 0) {
      return "FizzBuzz";
    }
    if (isDivisibleBy3) {
      return "Fizz";
    }
    if (isDivisibleBy5) {
      return "Buzz";
    }
    return Integer.toString(number);
  }
}
