package nl.fabiodamico.katas;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("This is a setup for test jUnit")
public class SetupTest {

  @Test
  @DisplayName("Should be always true")
  void shouldBeAlwaysTrue() {
    assertThat(true, is(true));
  }
}
