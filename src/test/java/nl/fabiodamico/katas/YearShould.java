package nl.fabiodamico.katas;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class YearShould {

  private YearCalculator yearCalculator;

  @BeforeEach
  void setUp() {
    yearCalculator = new YearCalculator();
  }

  @ParameterizedTest(name = "be not a leap year for the argument: {arguments}")
  @ValueSource(ints = {1997, 1999})
  void be_not_a_leap_year_when_not_divisible_by_four(int number) {
    boolean isLeapYear = yearCalculator.isLeapYear(number);
    assertThat(isLeapYear, is(false));
  }

  @ParameterizedTest(name = "be a leap year for the argument: {arguments}")
  @ValueSource(ints = {1996, 1992})
  void be_a_leap_year_when_divisible_by_four(int number) {
    boolean isLeapYear = yearCalculator.isLeapYear(number);
    assertThat(isLeapYear, is(true));
  }

  @ParameterizedTest(name = "be a leap year for the argument: {arguments}")
  @ValueSource(ints = {1600})
  void be_a_leap_year_when_divisible_by_fourhundred(int number) {
    boolean isLeapYear = yearCalculator.isLeapYear(number);
    assertThat(isLeapYear, is(true));
  }

  @ParameterizedTest(name = "be not a leap year for the argument: {arguments}")
  @ValueSource(ints = {1800})
  void be_not_a_leap_year_when_not_divisible_by_four_but_not_by_four_hundred(int number) {
    boolean isLeapYear = yearCalculator.isLeapYear(number);
    assertThat(isLeapYear, is(false));
  }
}
