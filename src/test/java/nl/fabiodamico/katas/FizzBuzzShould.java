package nl.fabiodamico.katas;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class FizzBuzzShould {

  private FizzBuzz fizzBuzz;

  @BeforeEach
  void setUp() {
    fizzBuzz = new FizzBuzz();
  }

  @ParameterizedTest(name = "print number: {arguments} as a string")
  @ValueSource(ints = {1, 2, 4, 7, 8, 11, 13})
  void print_number_as_string(int number) {
    String result = fizzBuzz.print(number);
    assertThat(result, is(String.valueOf(number)));
  }

  @Test
  @DisplayName("print Fizz when 3 is given")
  void print_Fizz_when_3_is_given() {
    String result = fizzBuzz.print(3);
    assertThat(result, is("Fizz"));
  }

  @Test
  @DisplayName("print Buzz when 5 is given")
  void print_Buzz_when_5_is_given() {
    String result = fizzBuzz.print(5);
    assertThat(result, is("Buzz"));
  }

  @Test
  @DisplayName("print FizzBuzz when input is divisible for 3 and 5")
  void print_FizzBuzz_when_input_is_divisible_for_3_and_5() {
    String result = fizzBuzz.print(15);
    assertThat(result, is("FizzBuzz"));
  }
}
