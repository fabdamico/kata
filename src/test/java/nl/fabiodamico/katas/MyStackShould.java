package nl.fabiodamico.katas;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MyStackShould {

  private MyStack<Integer> myStack;

  @BeforeEach
  void setUp() {
    myStack = new MyStack<Integer>();
  }

  @Test
  @DisplayName("create a valid MyStack Object")
  void create_valid_stackObj() {
    assertThat(myStack, is(notNullValue()));
  }

  @Test
  @DisplayName("create an empty MyStack Object")
  void create_empty_stack() {
    assertThat(myStack.getSize(), is(0));
  }

  @Test
  @DisplayName("contain one element after a push")
  void contain_one_element_after_push() {
    myStack.push(1);
    assertThat(myStack.getSize(), is(1));
  }

  @Test
  @DisplayName("contain zero element after one push and one pop")
  void contain_zero_element_after_one_push_and_one_pop() {
    myStack.push(1);
    myStack.pop();
    assertThat(myStack.getSize(), is(0));
  }

  @Test
  @DisplayName("throw IllegalStateException when a pop is performed on empty stack")
  void throw_an_exception_when_pop_is_performed_on_empty_stack() {
    assertThrows(IllegalStateException.class, () -> {
      myStack.pop();
    });
  }

  @Test
  @DisplayName("contain one element after two push and one pop")
  void contain_one_element_after_two_push_and_one_pop() {
    myStack.push(1);
    myStack.push(2);
    myStack.pop();
    assertThat(myStack.getSize(), is(1));
    assertThat(myStack.access(1), is(1));
  }

  @Test
  @DisplayName("contain pushed elements")
  void contain_pushed_elements() {
    myStack.push(1);
    myStack.push(2);
    myStack.push(3);
    myStack.push(4);
    myStack.push(5);
    myStack.push(6);
    myStack.pop();
    assertThat(myStack.contains(1), is(true));
    assertThat(myStack.contains(2), is(true));
    assertThat(myStack.contains(3), is(true));
    assertThat(myStack.contains(4), is(true));
    assertThat(myStack.contains(5), is(true));
    assertThat(myStack.contains(6), is(false));
    assertThat(myStack.getSize(), is(5));
  }

  @Test
  @DisplayName("return the elements we want to access")
  void return_the_elements_we_want_to_access() {
    myStack.push(34);
    myStack.push(128);
    int selectedElement = myStack.access(34);
    int selectedSecondElement = myStack.access(128);
    assertThat(selectedElement, is(34));
    assertThat(selectedSecondElement, is(128));
    assertThat(myStack.getSize(), is(2));
  }

  @Test
  @DisplayName("throw IllegalArgumentException when an element is not found in the stack")
  void throw_an_exception_when_element_is_not_found() {
    assertThrows(IllegalArgumentException.class, () -> {
      myStack.push(1);
      myStack.access(2);
    });
  }
}
